import React, { Component } from "react";
import "./App.css";
import Header from "./components/header";
import Input from "./components/input";

class App extends Component {
  render() {
    return (
      <div className="container">
        <main className="bg-container" id="bgContainer">
          <div className="main-bg-container">
            <Header />
            <Input />
          </div>
        </main>
      </div>
    );
  }
}

export default App;
