import React, { Component } from "react";
class Bottom extends Component {
  state = {};
  render() {
    return (
      <div className="status-container">
        <span> {this.props.counter} items left</span>
        <div className="sub-status-container">
          <button className="button" onClick={() => this.props.all(this.props.data)}>All</button>
          <button className="button" onClick={() => this.props.active(this.props.data)}>
            Active
          </button>
          <button className="button" onClick={() => this.props.completed(this.props.data)}>
            Completed
          </button>
        </div>
        <button className="button" onClick={() => this.props.clearCompleted(this.props.data)}>
          Clear Completed
        </button>
      </div>
    );
  }
}

export default Bottom;
