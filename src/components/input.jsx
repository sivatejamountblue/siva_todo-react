import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";
import Addtodo from "./addingTodo";
import Bottom from "./bottomContainer";
class Input extends Component {
  state = {
    data: [],
  };

  handleCounter = () => {
    return this.state.data.filter((item) => !item.isCompleted).length;
  };
  handleAll = () => {
    let allElement = document.querySelectorAll("li");
    for (let each of Array.from(allElement)) {
      each.parentElement.style.display = "flex";
    }
    this.setState({ data: this.state.data });
  };
  handleActive = (array) => {
    let activeElement = document.querySelectorAll("li");
    for (let each of Array.from(activeElement)) {
      if (each.classList.contains("strike")) {
        each.parentElement.style.display = "none";
      } else {
        each.parentElement.style.display = "flex";
      }
    }
    // this.state.data.map((each)=>{
    //   if(each.isCompleted){
    //     each.status= "no"
    //   }
    //   else{
    //     each.status = 'yes'
    //   }
    // })
    this.setState({ data: this.state.data });
  };
  handleCompleted = (event) => {
    let completedElement = document.querySelectorAll("li");
    for (let each of Array.from(completedElement)) {
      if (!each.classList.contains("strike")) {
        each.parentElement.style.display = "none";
      } else {
        each.parentElement.style.display = "flex";
      }
    }
    // this.state.data.map((each)=>{
    //   if(!each.isCompleted){
    //     each.status= "no"
    //   }
    //   else{
    //     each.status = 'yes'
    //   }
    // })
    this.setState({ data: this.state.data });
  };
  handleClearCompleted = () => {
    let clearCompletedElement = document.querySelectorAll("li");
    for (let each of Array.from(clearCompletedElement)) {
      if (each.classList.contains("strike")) {
        each.parentElement.remove();
      }
    }
    this.setState({ data: this.state.data });
  };

  addTodo = (event) => {
    if (event.key === "Enter") {
      if (event.target.value === "") {
        console.log("please add task");
      } else {
        let x = {
          id: uuidv4(),
          content: event.target.value,
          isCompleted: false,
          status: "yes",
        };
        this.setState((previousState) => ({
          data: [...previousState.data, x],
        }));
        event.target.value = "";
        console.log(this.state.data);
      }
    }
  };

  render() {
    return (
      <div className = "section-container">
        <div className="form-container">
          <input type="checkbox" />
          <input
            className="itemTextValue"
            id="todoValue"
            placeholder="Create a new todo..."
            type="text"
            onKeyDown={this.addTodo}
          />
        </div>
        <div className = "ulmain-container">
          <Addtodo data={this.state.data} />
          <Bottom
            all={this.handleAll}
            active={this.handleActive}
            completed={this.handleCompleted}
            clearCompleted={this.handleClearCompleted}
            counter={this.handleCounter()}
            data={this.state.data}
            checked={this.state.checked}
          />
        </div>
      </div>
    );
  }
}

export default Input;
