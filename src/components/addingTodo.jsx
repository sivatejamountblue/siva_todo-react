import React, { Component } from "react";
import '../App.css';
class Addtodo extends Component {
handleStrike = (event, item) => {
  if(event.target.checked){
    event.target.parentNode.classList.add('strike')
    item.isCompleted = item.isCompleted ? false : true;
      this.setState({data: this.props.data})

    }
    else{
        event.target.parentNode.classList.remove('strike')
        item.isCompleted = item.isCompleted ? false : true;
        this.setState({data: this.props.data})
    }
    this.setState({data: this.props.data})
}
handleDelete = (event) => {
    event.target.parentNode.remove()
    this.setState({data: this.props.data})
}

  render() {
    return (
      <ul className="ul-container">
        {this.props.data.filter(item => item.status === 'yes').map((item, index) => {
          return (
            <div className="li-container">
              <li className="input-check"> 
                <input onClick={(e)=>this.handleStrike(e,item)} key={index} type="checkbox" name="checkBox" id="inputCheck"  />
                <label className="label" key={item} htmlFor="inputCheck">{item.content}</label>
                <img onClick= {this.handleDelete} src="/images/icon-cross.svg" alt="" />
              </li>
            </div>
          );
        })}
      </ul>
    );
  }
}

export default Addtodo;
