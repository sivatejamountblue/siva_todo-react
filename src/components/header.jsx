import React, { Component } from "react";
import '../App.css';
class Header extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <header className="todo-heading-container mt-3 mb-3">
          <h2 className="heading">T O D O</h2>
          <img id="moonImage" src="images/icon-moon.svg" alt="" />
        </header>
      </React.Fragment>
    );
  }
}

export default Header;
